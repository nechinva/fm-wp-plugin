=== FM WordPress Plugin ===
Contributors: nechin
Tags: api, posts, pages
Requires at least: 5.0
Tested up to: 5.3
Stable tag: 1.0.0
Requires PHP: 7.3
License: GPLv2 or later

WordPress Plugin for test task.

== Description ==

Site front functionality:
The last inserted item added via the Web Interface should be pulled from the Service
and prepended to the top of every blog post, based on the currently selected list in the
plugin’s settings in the WP Admin dashboard.

WP Admin dashboard functionality:
a) A new menu item should be added on the left, labeled with “WP Test Menu”
b) When clicked, it should open the plugin’s settings page with a simple dropdown list with
3 items: list1, list2, list3. And a “Save” button.
c) Changing the currently selected list in the plugin’s settings should affect the data that
the plugin will show on the site front.

= 1.0.0 =
* Final variant

= 0.1.0 =
* Working variant

= 0.0.1 =
* Start
