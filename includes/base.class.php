<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Base class
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 27.11.2019, Vitkalov
 * @version 1.0
 */
class FM_WP_Base {
	/**
	 * @var null
	 */
	protected static $_instance = null;
	/**
	 * @var null
	 */
	public $common = null;
	/**
	 * @var null
	 */
	public $request = null;
	/**
	 * @var null
	 */
	public $settings = null;

	/**
	 * FM_WP_Base constructor
	 */
	public function __construct() {
		$this->includes();

		$this->common   = new FM_WP_Common();
		$this->request  = new FM_WP_Request();
		$this->settings = new FM_WP_Settings();
	}

	/**
	 * Get instance
	 *
	 * @return FM_WP_Base|null
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Include files
	 */
	private function includes() {
		require_once FM_WP_API_INCLUDES . 'common.class.php';
		require_once FM_WP_API_INCLUDES . 'request.class.php';
		require_once FM_WP_API_INCLUDES . 'settings.class.php';
	}

	/**
	 * Bind hooks
	 */
	private function bindHooks() {
		register_activation_hook( FM_WP_API_PATH, [ $this->common, 'activation' ] );
		register_uninstall_hook( FM_WP_API_PATH, [ 'FM_WP_Common', 'uninstall' ] );

		add_action( 'init', [ $this->common, 'init' ] );
		add_action( 'wp_enqueue_scripts', [ $this->common, 'adminEnqueueScripts' ] );

		add_action( 'admin_menu', [ $this->settings, 'createMenu' ] );
		add_action( 'admin_init', [ $this->settings, 'createSettings' ] );
	}

	/**
	 * Run execution
	 */
	public function run() {
		$this->bindHooks();
	}
}
