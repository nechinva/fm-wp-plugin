<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Request class
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 28.11.2019, Vitkalov
 * @version 1.0
 */
class FM_WP_Request {
	/**
	 * FM_WP_Request constructor
	 */
	public function __construct() {
	}

	/**
	 * Return headers array
	 *
	 * @return array
	 */
	private function getHeaders() {
		return [ 'auth-token' => FM_WP_API_TOKEN ];
	}

	/**
	 * Make get request with authorization headers and return response
	 *
	 * @param string $point Addition for request uri (/items/view/2)
	 * @param array $args Parameters for request
	 *
	 * @return array|WP_Error
	 */
	private function getApi( $point, $args = [] ) {
		$args['headers'] = $this->getHeaders();

		return wp_remote_get( FM_WP_API_URI . $point, $args );
	}

	/**
	 * Make post request with authorization headers and return response
	 *
	 * @param string $point Addition for request uri (/items/view/2)
	 * @param array $args Parameters for request
	 *
	 * @return array|WP_Error
	 */
	private function postApi( $point, $args = [] ) {
		$args['headers'] = $this->getHeaders();

		return wp_remote_post( FM_WP_API_URI . $point, $args );
	}

	/**
	 * Make special request with authorization headers and return response
	 *
	 * @param string $point Addition for request uri (/items/view/2)
	 * @param string $method Method for request (PUT or DELETE)
	 * @param array $args Parameters for request
	 *
	 * @return array|bool|WP_Error
	 */
	private function request( $point, $method, $args = [] ) {
		if ( $method != Requests::PUT && $method != Requests::DELETE ) {
			return false;
		}

		$args['method']  = $method;
		$args['headers'] = $this->getHeaders();

		return wp_remote_request( FM_WP_API_URI . $point, $args );
	}

	/**
	 * Check response
	 *
	 * @param array $response Response data
	 *
	 * @return bool
	 */
	public function check_response( $response ) {
		if (
			empty( $response )
			|| is_wp_error( $response )
			|| ! isset( $response['body'] )
			|| empty( $response['body'] )
			|| ( 200 != $response['response']['code'] && 201 != $response['response']['code'] )
		) {
			return false;
		}

		return true;
	}

	/**
	 * Check response data
	 *
	 * @param array|object $data Json decoded data
	 *
	 * @return bool
	 */
	public function check_data( $data ) {
		if (
			empty( $data )
			|| ( ! is_array( $data ) && ! is_object( $data ) )
			|| ! isset( $data->data )
		) {
			return false;
		}

		return true;
	}

	/**
	 * Get data from transient cache
	 *
	 * @param string $key md5 string
	 *
	 * @return mixed
	 */
	private function getCache( $key ) {
		return get_transient( $key );
	}

	/**
	 *  Set data to transient cache
	 *
	 * @param string $key md5 string
	 * @param mixed $value Mixed value
	 *
	 * @return bool
	 */
	private function setCache( $key, $value ) {
		return set_transient( $key, $value, MINUTE_IN_SECONDS );
	}

	/**
	 * Get the lists
	 *
	 * @return array
	 */
	public function getAllLists() {
		$point      = '/lists/index';
		$key        = md5( $point );
		$cachedData = $this->getCache( $key );
		if ( ! empty( $cachedData ) ) {
			return $cachedData;
		}

		$list = (object) [ 'id' => 0, 'name' => 'Select list' ];

		$json = $this->getApi( $point );
		if ( ! is_wp_error( $json ) && $json && $this->check_response( $json ) ) {
			$data = json_decode( $json['body'] );
			if ( $this->check_data( $data ) ) {
				array_unshift( $data->data, $list );

				$this->setCache( $key, $data->data );

				return $data->data;
			}
		}

		return [ $list ];
	}

	/**
	 * Get last item from the lists
	 *
	 * @param int $listId List Id
	 *
	 * @return string
	 */
	public function getLastItem( $listId ) {
		if ( empty( $listId ) ) {
			return '';
		}

		$point      = '/items/view/' . $listId . '/?last=1';
		$key        = md5( $point );
		$cachedData = $this->getCache( $key );
		if ( ! empty( $cachedData ) ) {
			return $cachedData;
		}

		$json = $this->getApi( $point );
		if ( ! is_wp_error( $json ) && $json && $this->check_response( $json ) ) {
			$data = json_decode( $json['body'] );
			if ( $this->check_data( $data ) ) {
				$this->setCache( $key, $data->data[0]->name );

				return $data->data[0]->name;
			}
		}

		return '';
	}
}