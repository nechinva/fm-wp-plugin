<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Settings page
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 28.11.2019, Vitkalov
 * @version 1.0
 */
class FM_WP_Settings {
	/**
	 * Smart_Eps_Settings constructor
	 */
	public function __construct() {
	}

	/**
	 * Return options with defaults
	 *
	 * @return mixed
	 */
	public function getFmWpOptions() {
		$default = [
			FM_WP_API_SLUG . '_list' => '0',
		];

		return get_option( FM_WP_API_OPTIONS, $default );
	}

	/**
	 * Create new menu item in settings menu
	 */
	public function createMenu() {
		add_menu_page(
			'Settings',
			'FM WP Settings',
			'manage_options',
			FM_WP_API_SLUG . '-settings',
			[
				$this,
				'showSettings',
			]
		);
	}

	/**
	 * Create new settings group
	 */
	public function createSettings() {
		register_setting( FM_WP_API_SLUG, FM_WP_API_OPTIONS );

		// Register a new section
		add_settings_section(
			FM_WP_API_SLUG . '_section_list',
			'FM WP Settings',
			[
				$this,
				'fmWpSectionListCp',
			],
			FM_WP_API_SLUG
		);

		// Lists
		add_settings_field(
			FM_WP_API_SLUG . '_list',
			'List',
			[
				$this,
				'fmWpFieldListSelectBox',
			],
			FM_WP_API_SLUG,
			FM_WP_API_SLUG . '_section_list',
			[
				'label_for' => FM_WP_API_SLUG . '_list',
			]
		);
	}

	/**
	 * Render select box list
	 *
	 * @param $args array
	 */
	public function fmWpFieldListSelectBox( $args ) {
		// Get the value of the setting we've registered with register_setting()
		$options = $this->getFmWpOptions();
		// Get the lists
		$lists = fmWpApiInstance()->request->getAllLists();
		// Output the field
		?>
        <select id="<?php echo esc_attr( $args['label_for'] ); ?>"
                name="<?php echo FM_WP_API_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
        >
			<?php foreach ( $lists as $list ) : ?>
                <option value="<?php echo $list->id; ?>" <?php echo isset( $options[ $args['label_for'] ] )
					? ( selected( $options[ $args['label_for'] ], $list->id, false ) )
					: ( '' ); ?>><?php echo $list->name; ?></option>
			<?php endforeach; ?>
        </select>
        <p class="description">Select list</p>
		<?php
	}

	/**
	 * Render hint
	 *
	 * @param $args array
	 */
	public function fmWpSectionListCp( $args ) {
		?>
        <p id="<?php echo esc_attr( $args['id'] ); ?>">Select the list for display on front page</p>
		<?php
	}

	/**
	 * Render settings page
	 */
	public function showSettings() {
		// Check user capabilities
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		// Check if the user have submitted the settings
		// WordPress will add the "settings-updated" $_GET parameter to the url
		if ( isset( $_GET['settings-updated'] ) ) {
			// Add settings saved message with the class of "updated"
			add_settings_error(
				FM_WP_API_SLUG . '_messages',
				FM_WP_API_SLUG . '_messages',
				'Settings saved',
				'updated'
			);
		}

		// Show error/update messages
		settings_errors( FM_WP_API_SLUG . '_messages' );
		?>
        <div class="wrap">
            <h1>Settings</h1>
            <form action="options.php" method="post">
				<?php
				// output fields for the registered setting
				settings_fields( FM_WP_API_SLUG );
				// output setting sections and their fields
				do_settings_sections( FM_WP_API_SLUG );
				// output save settings button
				submit_button( 'Save' );
				?>
            </form>
        </div>
		<?php
	}
}
