<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Common class
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 28.11.2019, Vitkalov
 * @version 1.0
 */
class FM_WP_Common {
	/**
	 * @var string Meta key name for post meta
	 */
	private $key = 'fm-wp-was-show';

	/**
	 * FM_WP_Hook constructor
	 */
	public function __construct() {
	}

	/**
	 * Activation plugin
	 */
	public function activation() {
	}

	/**
	 * Uninstall plugin
	 */
	public function uninstall() {
		unregister_setting( FM_WP_API_SLUG, FM_WP_API_OPTIONS );
	}

	/**
	 * Enqueue scripts
	 */
	public function adminEnqueueScripts() {
		wp_enqueue_style( 'fm-wp-api-style', FM_WP_API_URL . '/assets/css/style.css' );
	}

	/**
	 * Init wp
	 */
	public function init() {
		if ( ! is_admin() ) {
			add_action( 'loop_start', [ $this, 'loopStartPost' ] );
			add_filter( 'the_post', [ $this, 'filterPost' ], 10, 2 );
		}
	}

	/**
	 * Set start value for post meta
	 *
	 * @param object $query Current query
	 */
	public function loopStartPost( $query ) {
		foreach ( $query->posts as $post ) {
			update_post_meta( $post->ID, $this->key, false );
		}
	}

	/**
	 * Render content before post
	 *
	 * @param object $data Post data
	 * @param object $query Current query
	 */
	public function filterPost( $data, $query ) {
		global $post;

		$post_type = ! empty( $post ) ? $post->post_type : get_post( $data->ID )->post_type;
		if ( 'post' != $post_type ) {
			return;
		}

		$content = '';
		$listId  = fmWpApiInstance()->settings->getFmWpOptions()[ FM_WP_API_SLUG . '_list' ];

		if ( is_singular( [ $post_type ] ) && $post->ID == $data->ID ) {
			if ( did_action( 'get_header' ) && ! get_post_meta( $data->ID, $this->key, true ) ) {
				// Before single post
				$content = fmWpApiInstance()->request->getLastItem( $listId );
				update_post_meta( $data->ID, $this->key, true );
			}
		} else {
			if ( $query->post_count > 0 && ! get_post_meta( $data->ID, $this->key, true ) ) {
				// Before each post
				$content = fmWpApiInstance()->request->getLastItem( $listId );
				update_post_meta( $data->ID, $this->key, true );
			}
		}

		echo $content ? '<p class="fm-wp-center">' . $content . '</p>' : '';
	}
}
