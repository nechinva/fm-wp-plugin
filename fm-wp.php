<?php
/**
 * Plugin Name: FM Wordpress Plugin
 * Description: WordPress Plugin for test task
 * Version: 1.0.0
 * Author: Alexander Vitkalov
 * Author URI: http://vitkalov.ru
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'FM_WP_Base' ) ) {
	define( 'FM_WP_API_PATH', __FILE__ );
	define( 'FM_WP_API_DIR', dirname( __FILE__ ) . '/' );
	define( 'FM_WP_API_URL', plugins_url( null, __FILE__ ) );
	define( 'FM_WP_API_PLUGIN_BASE', plugin_basename( dirname( __FILE__ ) ) );

	define( 'FM_WP_API_INCLUDES', FM_WP_API_DIR . 'includes/' );

	define( 'FM_WP_API_SLUG', 'fm_wp_api' );
	define( 'FM_WP_API_OPTIONS', 'fm_wp_api_options' );

	define( 'FM_WP_API_URI', 'http://api-service.ru/api/v1' );
	define( 'FM_WP_API_TOKEN', '37f81c79396a9d88d5e21d72ae11eaf2151bdc84' );

	require_once FM_WP_API_INCLUDES . 'base.class.php';
}

try {
	$fm_wp_base = FM_WP_Base::instance();
	$fm_wp_base->run();
} catch ( Exception $e ) {
	exit( $e->getMessage() );
}

/**
 * Get the instance
 *
 * @return FM_WP_Base|null
 */
function fmWpApiInstance() {
	return FM_WP_Base::instance();
}
